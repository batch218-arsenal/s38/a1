const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name:{
		type: String,
		/*
			// Requires the data to be included when creating a record
			// The "true" value defined if the field is required or not, the second element in the array is the message that will be printed out in our terminal when the data is not present
		*/
		required: [true, "Course is required"]
	},
	description:{
		type: String,
		required: [true, "Description is required"]
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean

	},
	createdOn: {
		type: Date,
		// The "new Date()" expression instantiates a new "date" that the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})
							// Model Name
module.exports = mongoose.model("Course", courseSchema);