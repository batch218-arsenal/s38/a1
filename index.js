/*
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
	npm install bcrypt
		- "bcrypt" is a package for password hashing
	npm install jsonwebtoken
		- produces access token 
*/

// dependencies
const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/userRoute.js");
// to create an express server/application
const app = express();

// MIDDLEWARES - allows to bridge our backend application (server) to our front end

// to allow cross origin resource sharing
app.use(cors());

// to read json objects
app.use(express.json());

// to read forms
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.yovmeas.mongodb.net/course-booking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


//Prompts a message once connected
mongoose.connection.once('open', () => console.log('Now connected to Arsenal-Mongo DB Atlas.'));

// Prompts a message once connected to port 4000
app.listen(process.env.PORT || 4000, () => {console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

// 3000, 4000, 5000, 8000 - Port numbers for Web applications