const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens

// Token Creation
module.exports.createAccessToken = (user) => {

	// payload - data we want to include in our token
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
									//callback function
	// return jwt.sign(data, secret, {expiresIn : "60s"});

	//Generate a token
	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})
}